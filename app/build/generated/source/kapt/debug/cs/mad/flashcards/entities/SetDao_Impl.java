package cs.mad.flashcards.entities;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Class;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class SetDao_Impl implements SetDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<FlashcardSet> __insertionAdapterOfFlashcardSet;

  private final EntityDeletionOrUpdateAdapter<FlashcardSet> __deletionAdapterOfFlashcardSet;

  private final EntityDeletionOrUpdateAdapter<FlashcardSet> __updateAdapterOfFlashcardSet;

  public SetDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfFlashcardSet = new EntityInsertionAdapter<FlashcardSet>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `FlashcardSet` (`title`,`id`) VALUES (?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FlashcardSet value) {
        if (value.getTitle() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getTitle());
        }
        if (value.getId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindLong(2, value.getId());
        }
      }
    };
    this.__deletionAdapterOfFlashcardSet = new EntityDeletionOrUpdateAdapter<FlashcardSet>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `FlashcardSet` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FlashcardSet value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfFlashcardSet = new EntityDeletionOrUpdateAdapter<FlashcardSet>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `FlashcardSet` SET `title` = ?,`id` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FlashcardSet value) {
        if (value.getTitle() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getTitle());
        }
        if (value.getId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindLong(2, value.getId());
        }
        if (value.getId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, value.getId());
        }
      }
    };
  }

  @Override
  public void insert(final FlashcardSet flashInsert) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfFlashcardSet.insert(flashInsert);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final FlashcardSet flashDelete) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfFlashcardSet.handle(flashDelete);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final FlashcardSet flashUpdate) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfFlashcardSet.handle(flashUpdate);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public FlashcardSet[] getAll() {
    final String _sql = "Select * from FlashcardSet";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final FlashcardSet[] _result = new FlashcardSet[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final FlashcardSet _item;
        final String _tmpTitle;
        if (_cursor.isNull(_cursorIndexOfTitle)) {
          _tmpTitle = null;
        } else {
          _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        }
        final Long _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getLong(_cursorIndexOfId);
        }
        _item = new FlashcardSet(_tmpTitle,_tmpId);
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
