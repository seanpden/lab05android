package cs.mad.flashcards.activities;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import cs.mad.flashcards.entities.FlashDao;
import cs.mad.flashcards.entities.FlashDao_Impl;
import cs.mad.flashcards.entities.SetDao;
import cs.mad.flashcards.entities.SetDao_Impl;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class SetDatabase_Impl extends SetDatabase {
  private volatile SetDao _setDao;

  private volatile FlashDao _flashDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `FlashcardSet` (`title` TEXT NOT NULL, `id` INTEGER, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Flashcard` (`question` TEXT NOT NULL, `answer` TEXT NOT NULL, PRIMARY KEY(`question`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a1e4228bda3207fd07bea87e9419070')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `FlashcardSet`");
        _db.execSQL("DROP TABLE IF EXISTS `Flashcard`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsFlashcardSet = new HashMap<String, TableInfo.Column>(2);
        _columnsFlashcardSet.put("title", new TableInfo.Column("title", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcardSet.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFlashcardSet = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFlashcardSet = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFlashcardSet = new TableInfo("FlashcardSet", _columnsFlashcardSet, _foreignKeysFlashcardSet, _indicesFlashcardSet);
        final TableInfo _existingFlashcardSet = TableInfo.read(_db, "FlashcardSet");
        if (! _infoFlashcardSet.equals(_existingFlashcardSet)) {
          return new RoomOpenHelper.ValidationResult(false, "FlashcardSet(cs.mad.flashcards.entities.FlashcardSet).\n"
                  + " Expected:\n" + _infoFlashcardSet + "\n"
                  + " Found:\n" + _existingFlashcardSet);
        }
        final HashMap<String, TableInfo.Column> _columnsFlashcard = new HashMap<String, TableInfo.Column>(2);
        _columnsFlashcard.put("question", new TableInfo.Column("question", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcard.put("answer", new TableInfo.Column("answer", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFlashcard = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFlashcard = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFlashcard = new TableInfo("Flashcard", _columnsFlashcard, _foreignKeysFlashcard, _indicesFlashcard);
        final TableInfo _existingFlashcard = TableInfo.read(_db, "Flashcard");
        if (! _infoFlashcard.equals(_existingFlashcard)) {
          return new RoomOpenHelper.ValidationResult(false, "Flashcard(cs.mad.flashcards.entities.Flashcard).\n"
                  + " Expected:\n" + _infoFlashcard + "\n"
                  + " Found:\n" + _existingFlashcard);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "4a1e4228bda3207fd07bea87e9419070", "eb443d82e8f8b7fbe64c8c720d83cda6");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "FlashcardSet","Flashcard");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `FlashcardSet`");
      _db.execSQL("DELETE FROM `Flashcard`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(SetDao.class, SetDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(FlashDao.class, FlashDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
    final HashSet<Class<? extends AutoMigrationSpec>> _autoMigrationSpecsSet = new HashSet<Class<? extends AutoMigrationSpec>>();
    return _autoMigrationSpecsSet;
  }

  @Override
  public List<Migration> getAutoMigrations(
      @NonNull Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecsMap) {
    return Arrays.asList();
  }

  @Override
  public SetDao setDao() {
    if (_setDao != null) {
      return _setDao;
    } else {
      synchronized(this) {
        if(_setDao == null) {
          _setDao = new SetDao_Impl(this);
        }
        return _setDao;
      }
    }
  }

  @Override
  public FlashDao flashDao() {
    if (_flashDao != null) {
      return _flashDao;
    } else {
      synchronized(this) {
        if(_flashDao == null) {
          _flashDao = new FlashDao_Impl(this);
        }
        return _flashDao;
      }
    }
  }
}
