package cs.mad.flashcards.entities;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class FlashDao_Impl implements FlashDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<Flashcard> __insertionAdapterOfFlashcard;

  private final EntityDeletionOrUpdateAdapter<Flashcard> __deletionAdapterOfFlashcard;

  private final EntityDeletionOrUpdateAdapter<Flashcard> __updateAdapterOfFlashcard;

  public FlashDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfFlashcard = new EntityInsertionAdapter<Flashcard>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `Flashcard` (`question`,`answer`) VALUES (?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Flashcard value) {
        if (value.getQuestion() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getQuestion());
        }
        if (value.getAnswer() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getAnswer());
        }
      }
    };
    this.__deletionAdapterOfFlashcard = new EntityDeletionOrUpdateAdapter<Flashcard>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `Flashcard` WHERE `question` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Flashcard value) {
        if (value.getQuestion() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getQuestion());
        }
      }
    };
    this.__updateAdapterOfFlashcard = new EntityDeletionOrUpdateAdapter<Flashcard>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `Flashcard` SET `question` = ?,`answer` = ? WHERE `question` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Flashcard value) {
        if (value.getQuestion() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getQuestion());
        }
        if (value.getAnswer() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getAnswer());
        }
        if (value.getQuestion() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getQuestion());
        }
      }
    };
  }

  @Override
  public void insert(final Flashcard flashcardInsert) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfFlashcard.insert(flashcardInsert);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final Flashcard flashcardDelete) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfFlashcard.handle(flashcardDelete);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final Flashcard flashcardUpdate) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfFlashcard.handle(flashcardUpdate);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<Flashcard> getAll() {
    final String _sql = "Select * from Flashcard";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfQuestion = CursorUtil.getColumnIndexOrThrow(_cursor, "question");
      final int _cursorIndexOfAnswer = CursorUtil.getColumnIndexOrThrow(_cursor, "answer");
      final List<Flashcard> _result = new ArrayList<Flashcard>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Flashcard _item;
        final String _tmpQuestion;
        if (_cursor.isNull(_cursorIndexOfQuestion)) {
          _tmpQuestion = null;
        } else {
          _tmpQuestion = _cursor.getString(_cursorIndexOfQuestion);
        }
        final String _tmpAnswer;
        if (_cursor.isNull(_cursorIndexOfAnswer)) {
          _tmpAnswer = null;
        } else {
          _tmpAnswer = _cursor.getString(_cursorIndexOfAnswer);
        }
        _item = new Flashcard(_tmpQuestion,_tmpAnswer);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
