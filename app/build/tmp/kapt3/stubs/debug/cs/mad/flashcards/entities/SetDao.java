package cs.mad.flashcards.entities;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0006\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0013\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H\'\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0005H\'J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0005H\'\u00a8\u0006\r"}, d2 = {"Lcs/mad/flashcards/entities/SetDao;", "", "delete", "", "flashDelete", "Lcs/mad/flashcards/entities/FlashcardSet;", "getAll", "", "()[Lcs/mad/flashcards/entities/FlashcardSet;", "insert", "flashInsert", "update", "flashUpdate", "app_debug"})
public abstract interface SetDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "Select * from FlashcardSet")
    public abstract cs.mad.flashcards.entities.FlashcardSet[] getAll();
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.FlashcardSet flashInsert);
    
    @androidx.room.Update()
    public abstract void update(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.FlashcardSet flashUpdate);
    
    @androidx.room.Delete()
    public abstract void delete(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.FlashcardSet flashDelete);
}