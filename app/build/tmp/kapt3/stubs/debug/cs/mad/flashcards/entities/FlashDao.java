package cs.mad.flashcards.entities;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H\'J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0005H\'J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0005H\'\u00a8\u0006\f"}, d2 = {"Lcs/mad/flashcards/entities/FlashDao;", "", "delete", "", "flashcardDelete", "Lcs/mad/flashcards/entities/Flashcard;", "getAll", "", "insert", "flashcardInsert", "update", "flashcardUpdate", "app_debug"})
public abstract interface FlashDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "Select * from Flashcard")
    public abstract java.util.List<cs.mad.flashcards.entities.Flashcard> getAll();
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard flashcardInsert);
    
    @androidx.room.Update()
    public abstract void update(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard flashcardUpdate);
    
    @androidx.room.Delete()
    public abstract void delete(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard flashcardDelete);
}