package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.SetDao
import cs.mad.flashcards.runOnIO

class MainActivity : AppCompatActivity() {
    // Class var declarations
    private lateinit var binding: ActivityMainBinding
    private lateinit var setDao: SetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        // Usual commands (setView, inflate, etc)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Store a list of flashcardset
        var actualSet = listOf<FlashcardSet>() // stores a list of flashcardset in var

        // build a database instance
        val db = Room.databaseBuilder(
            applicationContext,
            SetDatabase::class.java,
            SetDatabase.databaseName
        ).build()

        // get dao from database
        setDao = db.setDao()

        // bind flashcardSetList to a list of flashcardset
        binding.flashcardSetList.adapter = FlashcardSetAdapter(actualSet)

        // createSetButton functionality
        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet("test"))

            // Inserts duplicate of addItem into database
            runOnIO {
                setDao.insert(flashInsert = FlashcardSet("test"))
            }


            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }

        // idk what this does
        // inserts a FlashcardSet into the DAO
//        runOnIO {
//            setDao.insert(flashInsert = FlashcardSet("insert"))
//        }

    }
}