package cs.mad.flashcards.activities

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.FlashDao
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.SetDao



@Database(entities = [FlashcardSet::class, Flashcard::class], version = 1)
abstract class SetDatabase : RoomDatabase() {

    companion object {
        const val databaseName = "FLASHCARDSET_DATABASE"
    }

    abstract fun setDao() : SetDao

    abstract fun flashDao(): FlashDao

}