package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.FlashDao
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.runOnIO

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var flashDao: FlashDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // Store a list of flashcard
        var actualSet = listOf<Flashcard>() // stores a list of flashcard in var

        // build a database instance
        val db = Room.databaseBuilder(
            applicationContext,
            SetDatabase::class.java,
            SetDatabase.databaseName
        ).build()

        // get dao from database
        flashDao = db.flashDao()

        binding.flashcardList.adapter = FlashcardAdapter(actualSet)

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard("test", "test"))

            // Inserts duplicate of addItem into database
            runOnIO {
                flashDao.insert(flashcardInsert = Flashcard("test", "test"))
            }

                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).deleteItem(Flashcard("test", "test"))

            // Inserts duplicate of addItem into database
            runOnIO {
                flashDao.delete(flashcardDelete = Flashcard("test", "test"))
            }

            startActivity(Intent(this, MainActivity::class.java))

        }


    }
}