package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class Flashcard(
    @PrimaryKey
    val question: String,
    @ColumnInfo
    val answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i"))
            }
            return cards
        }
    }
}
@Dao
interface FlashDao {

    @Query("Select * from Flashcard")
    fun getAll(): List<Flashcard>

    @Insert
    fun insert(flashcardInsert: Flashcard)

    @Update
    fun update(flashcardUpdate: Flashcard)

    @Delete
    fun delete(flashcardDelete: Flashcard)

}