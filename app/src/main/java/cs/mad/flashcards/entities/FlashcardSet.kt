package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class FlashcardSet(
    val title: String,
    @PrimaryKey
    val id: Long? = null)
    {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}

@Dao
interface SetDao {

    @Query("Select * from FlashcardSet")
    fun getAll(): Array<FlashcardSet>

    @Insert
    fun insert(flashInsert: FlashcardSet)

    @Update
    fun update(flashUpdate: FlashcardSet)

    @Delete
    fun delete(flashDelete: FlashcardSet)
}